# The Modelling of a Hysteresis Graph of Piezoelectric Elements using Deep Learning Bidirectional LSTM 




Abstract: This paper discusses and develop a deep learning model using bidirectional LSTM (long short-term memory) for predicting voltages necessary to stimulate a piezoelectric element to produce displacements in order to cancel or minimize vibrations. The predicted voltages rely on given displacements and time domain of the initial noise input. This noise input can then be ampliﬁed to match the resonance frequency of another piezoelectric element to generate the maximum voltage capable by this later piezoelectric element. This sinusoidal voltage then travels to a piezoelectric actuator to generate displacement that can cancel the initial noise. The model resulted a coefﬁcient of determination score  of 0.99983, a loss score of 0.0092 and MSE (mean squared error) of 8.5568e-05.





Author Contributions: Fawwaz Al-inizi: reviewed the literature, developed a deep learning model of the studied system, provided examples of integrating the model into real-life applications, evaluated the model, summarized the progressive models to reach the ﬁnal one, and edited the article. Marek Płaczek: Conceptualization, Founding acquisition, Validation; Supervision; Resources; Writing – review & editing. Andrzej Wróbel: Conceptualization, Validation; Supervision; Resources. Writing – review & editing. Jacek Harazin: Conceptualization, Validation; Supervision.


